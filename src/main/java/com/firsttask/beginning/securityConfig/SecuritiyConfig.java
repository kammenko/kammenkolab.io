package com.firsttask.beginning.securityConfig;

import com.firsttask.beginning.services.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@Configuration
@EnableWebSecurity
public class SecuritiyConfig  extends WebSecurityConfigurerAdapter {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    MyUserDetailsService myUserDetailsService;







//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//
//        http. headers().frameOptions().disable().and().
//                authorizeRequests()
//                .antMatchers("/**").permitAll()
//                .antMatchers("/home").permitAll()
//                .antMatchers("/registration").permitAll()
//                .antMatchers("/admin/**").hasRole("ADMIN").anyRequest()
//                .authenticated().and().csrf().disable().formLogin()
//                .loginPage("/login").failureUrl("/login?error=true")
//                .defaultSuccessUrl("/home")
//                .usernameParameter("email")
//                .passwordParameter("password")
//                .and().logout()
//                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//                .logoutSuccessUrl("/").and().exceptionHandling()
//                .accessDeniedPage("/access-denied");

    @Override
protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().headers().frameOptions().disable()
                .and().authorizeRequests()
                .antMatchers("/",
                        "/home", "/restDetails/**", "/groupOrders", "/downloadMenu/**",
                        "/showMenu/**", "/groupOrder/**", "/createOrder/**",
                        "/item/**", "/addItem/**", "/itemsList/**", "/orderDetails/**",
                        "/refresh/**", "/mail/send/**", "/api/**", "/restaurantOrders/**", "/addItems/**"



                        , "/restList").permitAll()
                .antMatchers("/admin/**")
                .access("hasAuthority('ADMIN')").anyRequest()
                .authenticated()
                .and().exceptionHandling().accessDeniedPage("/403").and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/home")
                .usernameParameter("email").passwordParameter("password")
                .failureUrl("/error").permitAll()
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/").permitAll();
    }
//   @Override
////   public void configureGlobal(AuthenticationManagerBuilder builder) throws Exception{
////
////        builder.userDetailsService();
////   }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(myUserDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
    }

}

