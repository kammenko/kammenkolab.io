package com.firsttask.beginning.mappers;

import com.firsttask.beginning.model.entities.Order;
import com.firsttask.beginning.model.entities.dto.OrderDTO;
import com.firsttask.beginning.repository.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OrdersMapper {
    @Autowired
    private RestaurantMapper restaurantMapper;
    @Autowired
    private ItemsMapper itemsMapper;
    @Autowired
    private OrdersRepository ordersRepository;
       // TO DTO
    public OrderDTO toDtoNoRes(Order order){
            OrderDTO orderDto = new OrderDTO();

            orderDto.setCreator(order.getCreator())
;
            orderDto.setEndTime(order.getEndTime());
            orderDto.setTimeout(order.getTimeout());
            orderDto.setId(order.getGuid());
            orderDto.setItems(itemsMapper.dtoListNoOrder(order.getItems()));
            orderDto.setOrderSent(order.isOrderSent());
        orderDto.setAuto(order.isAuto());

        return orderDto;
    }

    public OrderDTO toDtoRes(Order order){
        OrderDTO orderDto = this.toDtoNoRes(order);

        orderDto.setRestaurant(restaurantMapper.entToDto(order.getRestaurant()));




        return orderDto;
    }



        public List<OrderDTO> dtoListNoRest(List<Order> ordersList){

        List<OrderDTO> list = new ArrayList<>();
        for (int i = 0; i< ordersList.size(); i++){
            list.add(this.toDtoNoRes(ordersList.get(i)));

        }
        return list;
    }


    public List<OrderDTO> dtoLIstRes(List<Order> ordersList){

        List<OrderDTO> list = new ArrayList<>();
        for (int i = 0; i <ordersList.size() ; i++) {
            list.add(this.toDtoRes(ordersList.get(i)));
        }

        return list;
    }



    //TO ENT


    public Order toEnt(OrderDTO orderDTO){
        Order order = new Order();
        order.setGuid(orderDTO.getId());
        order.setRestaurant(restaurantMapper.dtoToEnt(orderDTO.getRestaurant()));
        order.setEndTime(orderDTO.getEndTime());
        order.setCreator(orderDTO.getCreator());
        order.setItems(order.getItems());
        order.setTimeout(orderDTO.getTimeout());
        order.setOrderSent(orderDTO.isOrderSent());
        order.setAuto(orderDTO.isAuto());
        if(ordersRepository.existsByGuid(orderDTO.getId()))
        order.setId(ordersRepository.findByGuid(orderDTO.getId()).getId());
        return order;

    }

}
