package com.firsttask.beginning.mappers;

import com.firsttask.beginning.model.entities.Item;
import com.firsttask.beginning.model.entities.dto.ItemDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ItemsMapper {

    @Autowired
    OrdersMapper ordersMapper;
    //TO DTO NO ORDER
    public ItemDTO toDtoNoOrder(Item item){
        ItemDTO itemDTO = new ItemDTO();

        itemDTO.setCreator(item.getCreator());
        itemDTO.setId(item.getGuid());
        itemDTO.setItem(item.getItem());
        itemDTO.setPrice(item.getPrice());

        return itemDTO;}



        public List<ItemDTO> dtoListNoOrder(List<Item> itemsList){
            List<ItemDTO> list=new ArrayList<>();
            for (int i = 0; i < itemsList.size(); i++) {
                list.add(this.toDtoNoOrder(itemsList.get(i)));
            }



           return list;
        }


        public Item toEnt(ItemDTO itemsDTO){

        Item item = new Item();
        item.setCreator(itemsDTO.getCreator());
        item.setItem(itemsDTO.getItem());
        item.setOrd(ordersMapper.toEnt(itemsDTO.getOrder()));
        item.setGuid(itemsDTO.getId());
        item.setPrice(itemsDTO.getPrice());

        return item;
        }

    }
