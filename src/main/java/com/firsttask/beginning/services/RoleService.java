package com.firsttask.beginning.services;

import com.firsttask.beginning.model.entities.Role;
import com.firsttask.beginning.repository.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Transactional

@EnableAsync
@EnableScheduling
public class RoleService implements IRoleService {
    @Autowired
    private RolesRepository repo;


    @Override
    public List<Role> findAll() {
        return repo.findAll();
    }

    @Override
    public void saveOrUpdate(Role object) {
        repo.save(object);
    }

    @Override
    public void delete(Role object) {

        repo.delete(object);
    }


    @Override
    public Role findByRola(String rola) {
        return repo.findByRola(rola);
    }
}
