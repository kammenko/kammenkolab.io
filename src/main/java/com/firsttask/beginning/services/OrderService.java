package com.firsttask.beginning.services;


import com.firsttask.beginning.mappers.OrdersMapper;
import com.firsttask.beginning.model.entities.Order;
import com.firsttask.beginning.model.entities.dto.ItemDTO;
import com.firsttask.beginning.model.entities.dto.OrderDTO;
import com.firsttask.beginning.repository.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@Service
@Transactional

@EnableAsync
@EnableScheduling
public class OrderService implements IOrdersService {

    @Autowired
    private OrdersRepository repo;
    @Autowired
    private OrdersMapper ordM;
    @Autowired
    private EmailService emailService;



    private ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();

    @Override
    public List<OrderDTO> findAll() {




        return ordM.dtoLIstRes(repo.findAll());
    }

    @Override
    public OrderDTO findByGuid(String id) throws NullPointerException{

        Order GroupO = repo.findByGuid(id);
            if(GroupO!=null){
                OrderDTO orderDTO = ordM.toDtoRes(repo.findByGuid(id));
//                double total = 0;
//                for (int i = 0; i < orderDTO.getItems().size() ; i++) {
//                    total+= orderDTO.getItems().get(i).getPrice();
//                }
//
//                orderDTO.setTotalPrice(total);
                return orderDTO ;}
            else throw new NullPointerException("Ne postoji order sa tim id-em");

    }



    @Override
    public OrderDTO saveOrUpdate(OrderDTO a)  {

            if(!repo.existsByGuid(a.getId()))
           a.setEndTime(Date.from(Instant.now().plusSeconds(a.getTimeout()*60)));
           a=ordM.toDtoRes(repo.save(ordM.toEnt(a)));

           emailService.sendTemplateMessage("kammenko@gmail.com", a);

        return a ;
    }

    @Override
    public void delete(OrderDTO object)  {

        if (repo.existsByGuid(object.getId()))



        repo.delete(repo.findByGuid(object.getId()));


    }


    @Override
    public double totalPrice(OrderDTO group) {
       double price = 0;
        for (ItemDTO order : group.getItems()
             ) { price= price + order.getPrice();

        }
    return price;

    }


}
