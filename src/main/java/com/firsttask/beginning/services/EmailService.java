package com.firsttask.beginning.services;

import com.firsttask.beginning.model.entities.dto.ItemDTO;
import com.firsttask.beginning.model.entities.dto.OrderDTO;

import java.util.List;

public interface EmailService {
    void sendSimpleMessage(String to, String subject, List<ItemDTO> list);
    void sendTemplateMessage(String to, OrderDTO order);
}
