package com.firsttask.beginning.services;


import com.firsttask.beginning.model.entities.Item;
import com.firsttask.beginning.model.entities.dto.ItemDTO;


import java.util.List;

public interface IItemsService /*extends Service<Users>*/ {

     List<ItemDTO> findAll();
     ItemDTO saveOrUpdate(ItemDTO object);
     //void delete(ItemsDTO object);

}

