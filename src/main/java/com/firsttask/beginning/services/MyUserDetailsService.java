package com.firsttask.beginning.services;

import com.firsttask.beginning.model.entities.Role;
import com.firsttask.beginning.model.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

//import org.springframework.security.core.userdetails.User;
@Service

@EnableAsync
@EnableScheduling
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    UserService userService;
//
//    @Override
//    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
//
//        User user = userService.findByEmail(s);
//        if(user == null)
//            throw new UsernameNotFoundException("User not found by email: " + s);
//
//        return toUserDetails(user);
//
//
//    }
//
//    private UserDetails toUserDetails(User user){
//        return org.springframework.security.core.userdetails.User.withUsername(user.getEmail())
//                .password(user.getPassword())
//                .roles(user.getRole().get(0).getRola()).build();
//
//
//    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userService.findByEmail(s);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }


        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : user.getRole()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getRola()));

        }

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), grantedAuthorities);
    }


}
