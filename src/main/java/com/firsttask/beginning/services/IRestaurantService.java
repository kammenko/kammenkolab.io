package com.firsttask.beginning.services;


import com.firsttask.beginning.model.entities.Restaurant;
import com.firsttask.beginning.model.entities.Restaurant;
import com.firsttask.beginning.model.entities.dto.RestaurantDTO;

import java.util.List;
import java.util.Optional;

public interface IRestaurantService /*extends Service<Users>*/ {

     List<RestaurantDTO> findAll();
     void save(RestaurantDTO object) throws Exception;
     void delete(RestaurantDTO object);
     Restaurant findByRName(String name);
     RestaurantDTO findByGuid(String id);
     boolean doesExiist(Restaurant object);
     void update(RestaurantDTO restaurantDTO);


}
