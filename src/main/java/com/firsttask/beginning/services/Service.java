package com.firsttask.beginning.services;

import java.util.List;

public interface Service<T> {

    List<T> findAll();
    void saveOrUpdate(T object);
     void delete(T object);

}
