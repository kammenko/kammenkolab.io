package com.firsttask.beginning.services;


import com.firsttask.beginning.model.entities.Role;

import java.util.List;

public interface IRoleService /*extends Service<Users>*/ {

     List<Role> findAll();
     void saveOrUpdate(Role object);
     void delete(Role object);
     Role findByRola(String rola);
}
