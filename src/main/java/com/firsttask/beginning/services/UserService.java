package com.firsttask.beginning.services;

import com.firsttask.beginning.mappers.UserMapper;
import com.firsttask.beginning.model.entities.User;
import com.firsttask.beginning.model.entities.dto.UserDTO;
import com.firsttask.beginning.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Transactional

@EnableAsync
@EnableScheduling
public class UserService implements IUserService {

    @Autowired
    private UsersRepository repo;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public List<User> findAll() {

        return repo.findAll() ;
    }

    @Override

    public UserDTO saveOrUpdate(UserDTO object) {
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
       return userMapper.entToDTO(repo.save(userMapper.dtoToEnt(object)));
    }

    @Override
    public void delete(User object) {
    repo.delete(object);
    }


    @Override
    public User findByEmail(String email) {
        return repo.findByEmail(email);

    }
}
