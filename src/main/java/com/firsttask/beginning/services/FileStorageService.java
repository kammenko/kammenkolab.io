package com.firsttask.beginning.services;

import com.firsttask.beginning.property.FileStorageProperties;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

@Service
@Transactional

@EnableAsync
@EnableScheduling
public class FileStorageService implements StorageService {

    private final Path fileStorageLocation;


    @Autowired
    public FileStorageService(FileStorageProperties properties) {

        this.fileStorageLocation = Paths.get(properties.getUploadDir());
    }


    @Override
    public void init() throws Exception {
        try {
            Files.createDirectories(fileStorageLocation);
        } catch (IOException ex) {
            throw new Exception("Couldn't create directory", ex);
        }

    }

    @Override
    public String store(MultipartFile file, String name) throws Exception {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String ext = FilenameUtils.getExtension(fileName);

                try {
            if (file.isEmpty()) {
                throw new Exception("File " + fileName + " is empty and it couldnt be saved");
            }
            if (fileName.contains("..")) {
                throw new Exception("Cannot store file with relative path outside current directory "
                        + fileName);
            }

            InputStream inputStream = file.getInputStream();

            Files.copy(inputStream, this.fileStorageLocation.resolve(name + "_Menu."+ext), StandardCopyOption.REPLACE_EXISTING);
                return name+"_Menu."+ext;

        } catch (IOException ex) {

            throw new Exception("Failed to store file " + fileName, ex);

        }


    }

    @Override
    public Stream<Path> loadAll() {
        return null;
    }

    @Override
    public Path load(String fileName) {
        return fileStorageLocation.resolve(fileName);
    }

    @Override
    public Resource loadAsResource(String fileName) throws Exception {
        try {
            Path file = load(fileName);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else throw new Exception("Could not read file: " + fileName);
        } catch (MalformedURLException ex) {
            throw new Exception("Could not read file: " + fileName);
        }


    }

    @Override
    public void deleteAll() {

    }

    @Override
    public void delete(String fileName) {
        Path file = load(fileName);
        File fileToDelete = file.toFile();
        fileToDelete.delete();
    }
}
