package com.firsttask.beginning.services;


import com.firsttask.beginning.model.entities.User;
import com.firsttask.beginning.model.entities.dto.UserDTO;

import java.util.List;

public interface IUserService /*extends Service<Users>*/ {

    List<User> findAll();
     UserDTO saveOrUpdate(UserDTO object);
     void delete(User object);
     User findByEmail(String email);
}
