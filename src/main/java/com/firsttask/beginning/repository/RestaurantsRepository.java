package com.firsttask.beginning.repository;

import com.firsttask.beginning.model.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantsRepository extends JpaRepository<Restaurant, Long> {
    @Query("Select r from Restaurant r where LOWER(r.name) = lower(:name)")
    Restaurant findByRName(@Param("name") String name);

    @Query(" select r from Restaurant r where" +
            " LOWER(r.name) = LOWER(:name) or " +
            "lower(r.adress) = lower(:adress) or r.phoneNo = :phone_no" +
            " or lower(r.menuUrl) = lower(:menu_url) ")
    Restaurant existanceCheck(@Param("name") String name,@Param("adress")String adress,
                      @Param("phone_no") String phoneNo, @Param("menu_url")String menuUrl);


    Restaurant findByAdress(String adress);
    Restaurant findByPhoneNo(String phoneNo);
    Restaurant findByMenuUrl(String menuUrl);
    Restaurant findByEmail(String email);
    Restaurant findByGuid(String guid);
}
