package com.firsttask.beginning.repository;

import com.firsttask.beginning.model.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesRepository extends JpaRepository<Role, Long> {
    @Query("Select r from Role r where LOWER(r.role) = lower(:rola)")
    Role findByRola(@Param("rola") String rola);

}
