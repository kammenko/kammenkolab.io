//package com.firsttask.beginning.controllers;
//
//import com.firsttask.beginning.model.entities.*;
//import com.firsttask.beginning.services.GroupOrderService;
//import com.firsttask.beginning.services.RestaurantService;
//import com.firsttask.beginning.services.RoleService;
//import com.firsttask.beginning.services.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.validation.Valid;
//import java.util.ArrayList;
//import java.util.List;
//
//@Controller
//@RequestMapping(value = "/admin/" )
//public class AdminController {
//    @Autowired
//    UserService userS;
//    @Autowired
//    RestaurantService restS;
//    @Autowired
//    RoleService roleS;
//    @Autowired
//    GroupOrderService groupO;
//
//
//    @GetMapping(value = "regUsers")
//    public ModelAndView checkUsers(ModelAndView model){
//
//        model.setViewName("users");
//        model.addObject("userList", userS.findAll());
//
//    return model;
//    }
//
//    @GetMapping(value = "restaurantAdd")
//    public ModelAndView addRest(ModelAndView model) {
//
//
//        model.addObject("newRest", new Restaurants());
//        model.addObject("resButt", "Save");
//
//
//        model.setViewName("restaurantAdd");
//
//        return model;
//    }
//
//    @PostMapping(value = "restaurantAdd")
//    public ModelAndView addRests(ModelAndView model,@Valid Restaurants restaurant,  BindingResult bindingResult)
//    {
//        Restaurants rest= new Restaurants();
//
//       // rest = restS.findByRName(restaurant.getRname());
//        if (rest != null) {
//
//            model.addObject("restError", "Vec Postoji restoran sa tim atributima");
//            model.addObject("newRest", new Restaurants());
//            model.addObject("resButt", "Save");
//            model.setViewName("restaurantAdd");
//
//        }
//
//
//        else {
//
//            restS.saveOrUpdate(restaurant);
//            model.addObject("succesMessage", "Uspesno ste se registrovali");
//            model.addObject("newRest", new Restaurants());
//            model.addObject("resButt", "Save");
//
//            model.setViewName("restaurantAdd");
//        }
//
//
//        return model;
//    }
//
//    @RequestMapping(value = "restaurantList")
//    public ModelAndView listRest(ModelAndView model){
//        model.addObject("restaurantList", restS.findAll());
//        model.addObject("groupOrder", new GroupOrders());
//        model.addObject("newRest", new Restaurants());
//        model.setViewName("restaurantList");
//        return model;
//    }
//
//    @GetMapping(value = "restaurantEdit/{id}")
//    public ModelAndView editRest(ModelAndView model, @PathVariable long id){
//        model.addObject("newRest", restS.findById(id).get());
//        model.setViewName("restaurantAdd");
//        model.addObject("resButt", "Update");
//
//
//        return model;
//
//    }
//
//    @GetMapping(value = "changeRole/{email}")
//    public ModelAndView changeRole(ModelAndView model, @PathVariable String email){
//        Users user = userS.findByEmail(email);
//        List<Roles> list = new ArrayList<>();
//        if (user.getRole().size()<1){
//           // user.addRole(roleS.findByRola("ANONYMOUS"));
//
//        }
////        if (user.getRole().get(0).getRola().equals("ADMIN"))
////        {
////            list.add(roleS.findAll().get(1));
////            user.setRole(list);
////            userS.saveOrUpdate(user);
////        }
////        else{
////            list.add(roleS.findAll().get(0));
////            user.setRole(list);
////            userS.saveOrUpdate(user);
////
////        }
//        model.addObject("userList", userS.findAll());
//                model.setViewName("users");
//    return model;}
//
//    @PostMapping(value = "restaurantUpdate")
//    public ModelAndView updateRes(@ModelAttribute("newRest") Restaurants restaurant){
//
//        restS.saveOrUpdate(restaurant);
//
//
//        return new ModelAndView("redirect:/admin/restaurantList");
//    }
//
//    @RequestMapping(value = "deleteRestaurant/{id}")
//    public ModelAndView deleteRes(@PathVariable long id){
//
//        restS.delete(restS.findById(id).get());
//
//        return new ModelAndView("redirect:/admin/restaurantList");}
//
//    @RequestMapping(value = "go")
//    public ModelAndView test(){
//            ModelAndView model = new ModelAndView();
//            model.addObject("order", groupO.findById(99));
//        model.addObject("orders", new Orders());
//            model.setViewName("groupOrderV2");
//        return model;
//    }
//
//}
//
//
//
