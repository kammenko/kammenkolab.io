package com.firsttask.beginning.controllers;

import com.firsttask.beginning.exceptions.OrderExpiredException;
import com.firsttask.beginning.model.entities.Item;
import com.firsttask.beginning.model.entities.User;
import com.firsttask.beginning.model.entities.dto.ItemDTO;
import com.firsttask.beginning.model.entities.dto.OrderDTO;
import com.firsttask.beginning.model.entities.dto.RestaurantDTO;
import com.firsttask.beginning.services.*;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.net.URLConnection;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@Controller
@RequestMapping(value = "/")
public class EmployeeControl {


    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    private RestaurantService restS;
    @Autowired
    private OrderService orderS;
    @Autowired
    private ItemsService itemS;
    @Autowired
    private EmailService emailService;




    @GetMapping(value = {"/", "home"})
    public ModelAndView home() {

        ModelAndView model = new ModelAndView();
        model.addObject("user", new User());
        model.setViewName("home");

        return model;
    }
        //RESTAURANTS/EMPLOYEE CONTROLS// todo check
    @GetMapping(value = "restList")
    public ModelAndView restList() {

        ModelAndView model = new ModelAndView();
        model.addObject("user", new User());
        model.addObject("restaurants", restS.findAll());
        model.addObject("Order", new OrderDTO());
        model.setViewName("restaurantList");
        return model;
    }

    @GetMapping(value = "restDetails/{id}")
    public ModelAndView editRes(@PathVariable String id) {

        RestaurantDTO rest = restS.findByGuid(id);
        ModelAndView model = new ModelAndView();
        model.addObject("rest", rest);
        model.setViewName("RestaurantDetails");
        model.addObject("resButt", "Update");
        return model;

    }


    @GetMapping(value = "restaurantOrders/{id}")
    public ModelAndView ordersForRestaurant(@PathVariable String id) {

        ModelAndView model = new ModelAndView();
        model.addObject("groupOrderList", restS.findByGuid(id).getGroupOrders());
        model.addObject("restaurant", restS.findByGuid(id).getName());
        model.addObject("item", new ItemDTO());
        model.setViewName("groupOrderList");
        return model;
    }


        //ORDERS CONTROLL//todo check
    @GetMapping(value = "groupOrders")
    public ModelAndView orders() {

        ModelAndView model = new ModelAndView();
        model.addObject("groupOrderList", orderS.findAll());
        model.addObject("item", new ItemDTO());
        model.setViewName("groupOrderList");

        return model;
    }







    @GetMapping(value = "groupOrder/{id}")
    public ModelAndView group(@PathVariable String id) {

        ModelAndView model = new ModelAndView();
        OrderDTO order = new OrderDTO();
        order.setRestaurant(restS.findByGuid(id));
        model.addObject("rest", restS.findByGuid(id));
        model.addObject("Order", order);
        model.setViewName("groupOrders");

        return model;
    }

    @PostMapping(value = "createOrder/{id}")
    public ModelAndView createOrder(ModelAndView model, @Valid OrderDTO order, @PathVariable String id) {

        order.setId(UUID.randomUUID().toString());
        order.setRestaurant(restS.findByGuid(id));
        order = orderS.saveOrUpdate(order);
        model.setViewName("redirect:/groupOrders");

        return model;
    }

    @GetMapping(value = "orderDetails/{id}")
    public ModelAndView orderDetails(@PathVariable String id) throws Exception {

        ModelAndView model = new ModelAndView();
        OrderDTO order = orderS.findByGuid(id);
        order.setTotalPrice(orderS.totalPrice(order));
        if (order.getEndTime().after(Date.from(Instant.now())))
            order.setTimeLeft((order.getEndTime().getTime() - Date.from(Instant.now()).getTime()) / 1000);
        else order.setTimeLeft(0);
        model.addObject("order", order);
        model.addObject("Item", new Item());
        model.setViewName("groupOrder");


        return model;
    }



    //ITEM CONTROLS//todo check
    @GetMapping(value = "item/{id}")
    public ModelAndView itemForm(@PathVariable String id) {

        ModelAndView model = new ModelAndView();
        model.addObject("id", id);
        model.addObject("Item", new Item());
        model.setViewName("order");

        return model;
    }



    @PostMapping(value = "addItem/{id}")
    public ModelAndView addItem(@Valid ItemDTO item, BindingResult bindingResult, @PathVariable String id, ModelAndView model) throws OrderExpiredException {
        if(bindingResult.hasErrors()){

            model.addObject("groupOrderList", orderS.findAll());
            model.addObject("item", new ItemDTO());
            model.setViewName("groupOrderList");
        return model;}
        item.setOrder(orderS.findByGuid(id));
        if (item.getOrder().getEndTime().after(new Date()))
            itemS.saveOrUpdate(item);
        else throw new OrderExpiredException("Porudzbina je istekla");
        model.setViewName("redirect:/groupOrders");

        return model;
    }

    @PostMapping(value = "addItems/{id}")
    public ModelAndView addItems(ItemDTO item, @PathVariable String id, ModelAndView model) {

        item.setOrder(orderS.findByGuid(id));

        itemS.saveOrUpdate(item);
        model.setViewName("redirect:/orderDetails/" + id);

        return model;
    }

    @GetMapping(value = "itemsList/{id}")
    public ModelAndView itemsList(@PathVariable String id) {
        ModelAndView model = new ModelAndView();
        model.addObject("id", id);
        model.addObject("itemsList", orderS.findByGuid(id).getItems());
        model.addObject("totalPrice", orderS.totalPrice(orderS.findByGuid(id)));
        model.setViewName("ordersList");

        return model;

    }



    //FILE CONTROLS//todo check
    @GetMapping(value = "downloadMenu/{fileName}")
    @ResponseBody
    public ResponseEntity<Resource> download(@PathVariable String fileName) throws Exception {

        Resource file = fileStorageService.loadAsResource(fileName);

        return
                ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                        .contentType(MediaType.parseMediaType("application/" + FilenameUtils.getExtension(file.getFilename()))).body(file);


    }

    @GetMapping(value = "showMenu/{fileName}")
    @ResponseBody
    public ResponseEntity<Resource> show(@PathVariable String fileName) throws Exception {

        Resource file = fileStorageService.loadAsResource(fileName);

        String mimeType = URLConnection.guessContentTypeFromName(file.getFilename());
        if (mimeType == null) {
            //unknown mimetype so set the mimetype to application/octet-stream
            mimeType = "application/octet-stream";


        }

        return
                ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + file.getFilename() + "\"")
                        .contentType(MediaType.valueOf(mimeType))

                        .body(file);


    }



}
