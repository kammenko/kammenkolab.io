package com.firsttask.beginning.controllers;


import com.firsttask.beginning.model.entities.dto.ItemDTO;
import com.firsttask.beginning.services.OrderService;
import com.firsttask.beginning.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/")
public class REstControl {
    @Autowired
    private OrderService gOService;
    @Autowired
    private RestaurantService rest;


    @GetMapping(value = "ss/{id}")
    public List<ItemDTO> allRest(@PathVariable String id) {
        return gOService.findByGuid(id).getItems();
    }




}