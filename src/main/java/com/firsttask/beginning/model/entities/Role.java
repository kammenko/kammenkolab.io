package com.firsttask.beginning.model.entities;


import javax.persistence.*;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Role.findByRola", query = "select  r from Role r where r.role = :rola")}
)
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
   private long id;



    @Column(name = "rola",unique = true)

    private String role;

    @ManyToMany(mappedBy = "role")
    private List<User> usersList;

    public Role() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRola() {
        return role;
    }

    public void setRola(String rola) {
        this.role = rola;
    }

    public List<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }
}
