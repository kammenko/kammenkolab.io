package com.firsttask.beginning.model.entities.dto;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;

public class RestaurantDTO {

    private String id;
    @Size(max = 100)
    @NotNull
    @NotBlank(message = "this is a required field and it cannot be empty")
    private String name;
    @Size(max = 100)
    @NotNull
    @NotBlank(message = "This is a required field and it cannot be empty")
    @Size(min = 5 , message = "Adress must be at least 5 characters long")
    private String adress;
    @Size(max = 20)
    @NotNull
    @NotBlank(message = "This is a required field and it cannot be empty")
    @Pattern(regexp = "06[0-9]{1}-[0-9]{3}-[0-9]{4}", message = "Phone number must be in this format 06x-xxx-xxxx")
    private String phoneNo;
    @Size(max = 500)
    @NotNull
    @NotBlank(message = "This is a required field and it cannot be empty")
    private String menuUrl;
    @Size(max = 2000, message = "Delivery info size must be shorter than 2000 characters")
    private String deliveryInfo;
    @Email(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])" )

    private String email;

    private List<OrderDTO> groupOrders = new ArrayList<>();

    public RestaurantDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phone_no) {
        this.phoneNo = phone_no;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menu_url) {
        this.menuUrl = menu_url;
    }

    public String getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(String delivery_info) {
        this.deliveryInfo = delivery_info;
    }

    public List<OrderDTO> getGroupOrders() {
        return groupOrders;
    }

    public void setGroupOrders(List<OrderDTO> groupOrders) {
        this.groupOrders = groupOrders;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
