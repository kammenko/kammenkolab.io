package com.firsttask.beginning.model.entities.dto;



import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ItemDTO {

    private String id;
    @Size(max = 45)
    @NotNull
    @NotBlank( message = "This field cant be empty")
    private String creator;
    @NotNull
    private double price;

    @Size(max = 50)
    @NotNull
    @NotBlank(message = "You must fill out this field")
    private String item;

    private OrderDTO order;

    public ItemDTO() {
    }

    public ItemDTO(String id, String creator, double price, String item, OrderDTO order) {
        this.id = id;
        this.creator = creator;
        this.price = price;
        this.item = item;
        this.order = order;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public OrderDTO getOrder() {
        return order;
    }

    public void setOrder(OrderDTO order) {
        this.order = order;
    }
}
