
CREATE TABLE restaurants(
id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,

adress VARCHAR(100) NOT NULL,
phone_no VARCHAR(20) NOT NULL ,
menu_url VARCHAR(500) NOT NULL,
delivery_info VARCHAR(2000)
 );

CREATE TABLE orders (
id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
creator VARCHAR(45),
timeout INT DEFAULT '10' ,
restaurant BIGINT(20) NOT NULL,
end_time datetime NOT NULL,
CONSTRAINT fk1 FOREIGN KEY (restaurant) REFERENCES restaurants (id) ON DELETE CASCADE
);



CREATE TABLE roles(
id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
rola VARCHAR(45) NOT NULL UNIQUE
);

CREATE TABLE users(
id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
password VARCHAR(200) NOT NULL,
first_name VARCHAR(200) NULL ,
last_name VARCHAR(200) NULL,
email VARCHAR(200) NOT NULL UNIQUE

);

CREATE TABLE users_roles(
users BIGINT(20) NOT NULL,
roles BIGINT(20) NOT NULL,
PRIMARY KEY(users, roles),
 constraint fk3
  foreign key (users) references users (id)
    on delete cascade,
  constraint fk4
  foreign key (roles) references roles (id)
);


insert into users values (1, '$2a$04$iLCOUtNxVnkIlheVpyuCiuJKS.ZyHSEj3EY6OWywsfgol33h84uTS', 'Admin', 'admin', 'admin@admin.com');
insert into roles values (1, 'ADMIN');
insert into users_roles values (1,1);

