
CREATE TABLE if not exists roles (
	role_id int NOT NULL AUTO_INCREMENT,
	rola varchar(45) NOT NULL UNIQUE,
	PRIMARY KEY (role_id)
);

CREATE TABLE if not exists users (
	user_id int NOT NULL AUTO_INCREMENT,
	username varchar(45) NOT NULL UNIQUE,
	password varchar(200) NOT NULL,
	first_name varchar(200),
	last_name varchar(200),
	role_id int default 2,
	PRIMARY KEY (user_id)
);

CREATE TABLE if not exists restaurants (
	restaurant_id int NOT NULL AUTO_INCREMENT,
	rname varchar(100) NOT NULL UNIQUE,
	adress varchar(100) NOT NULL UNIQUE,
	phone_number varchar(20) NOT NULL UNIQUE,
	menu_url varchar(500) NOT NULL UNIQUE,
	delivery_info varchar(2000),
	PRIMARY KEY (restaurant_id)
);

CREATE TABLE if not exists groupOrders (
	group_id int NOT NULL AUTO_INCREMENT,
	creator varchar(45) NOT NULL,
	timeout int DEFAULT 10,
	restaurant int NOT NULL ,
	PRIMARY KEY (group_id)
);

CREATE TABLE  if not exists orders (
	order_id int NOT NULL AUTO_INCREMENT,
	rname varchar(45) NOT NULL,
	price int NOT NULL,
	items varchar(200) NOT NULL,
	PRIMARY KEY (order_id)
);

CREATE TABLE if not exists lists (
	groups int NOT NULL,
	orders int NOT NULL
);

 ALTER TABLE users ADD CONSTRAINT Users_fk0 FOREIGN KEY (role_id) REFERENCES roles(role_id);

ALTER TABLE groupOrders ADD CONSTRAINT GroupOrder_fk0 FOREIGN KEY (restaurant) REFERENCES restaurants(restaurant_id);

ALTER TABLE lists ADD CONSTRAINT List_fk0 FOREIGN KEY (groups) REFERENCES groupOrders(group_id);

ALTER TABLE lists ADD CONSTRAINT List_fk1 FOREIGN KEY (orders) REFERENCES orders(order_id);



